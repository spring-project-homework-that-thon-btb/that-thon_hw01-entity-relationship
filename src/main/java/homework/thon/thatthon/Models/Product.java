package homework.thon.thatthon.Models;

import com.fasterxml.jackson.annotation.JsonIgnore;

import javax.persistence.*;
import java.util.ArrayList;
import java.util.List;

@Entity
public class Product {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer id;
    private String name;
    private Integer qty;
    private Double price;

    @ManyToMany(cascade = CascadeType.ALL,fetch = FetchType.LAZY)
    @JoinTable(name = "purchase_order_detail")
    @JsonIgnore
    private List<PurchaseOrder> purchaseOrders=new ArrayList<>();

    public Product() {
    }

    public Product(String name, Integer qty, Double price) {
        this.name = name;
        this.qty = qty;
        this.price = price;
    }

    public Product(String name, Integer qty, Double price, List<PurchaseOrder> purchaseOrders) {
        this.name = name;
        this.qty = qty;
        this.price = price;
        this.purchaseOrders = purchaseOrders;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Integer getQty() {
        return qty;
    }

    public void setQty(Integer qty) {
        this.qty = qty;
    }

    public Double getPrice() {
        return price;
    }

    public void setPrice(Double price) {
        this.price = price;
    }

    public List<PurchaseOrder> getPurchaseOrders() {
        return purchaseOrders;
    }

    public void setPurchaseOrders(List<PurchaseOrder> purchaseOrders) {
        this.purchaseOrders = purchaseOrders;
    }

}
