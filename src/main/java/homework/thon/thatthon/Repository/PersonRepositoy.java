package homework.thon.thatthon.Repository;

import homework.thon.thatthon.Models.Person;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.transaction.Transactional;
import java.util.List;

@Repository
@Transactional
public class PersonRepositoy {

    @PersistenceContext
    private EntityManager entityManager;

    public void save(Person person) {
        entityManager.persist(person);
    }

    public Person modify(Integer id) {
        Person person = entityManager.find(Person.class, id);
        person.setName("Spring");
        person.setGender("Female");
        person.setEmail("Spring@gmail.com");
        entityManager.flush();
        return null;

    }

    public Person remove(Integer id) {
        Person person=entityManager.find(Person.class,id);
        entityManager.remove(person);
        return null;
    }

    public List<Person> allPerson() {
        return entityManager.createQuery("select p from Person p").getResultList();
    }


}
