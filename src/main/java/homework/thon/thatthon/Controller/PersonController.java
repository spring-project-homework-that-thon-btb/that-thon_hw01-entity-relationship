package homework.thon.thatthon.Controller;

import homework.thon.thatthon.Models.Address;
import homework.thon.thatthon.Models.Person;
import homework.thon.thatthon.Models.School;
import homework.thon.thatthon.Repository.PersonRepositoy;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import java.util.List;

@Controller
@ResponseBody
@RequestMapping("/person")
public class PersonController {

    private PersonRepositoy personRepositoy;

    @Autowired
    public void setPersonRepositoy(PersonRepositoy personRepositoy) {
        this.personRepositoy = personRepositoy;
    }

    @GetMapping
    private List<Person> allPerson(){
        return personRepositoy.allPerson();
    }

    @GetMapping("/save")
    private void save(){
        Address address=new Address("PP");
        Person person=new Person("Dara","Male","dara@gmail.com",address);

        School school1=new School("SETEC");
        School school2=new School("RUPP");
        School school3=new School("AUCC");
        person.addSchool(school1);
        person.addSchool(school2);
        person.addSchool(school3);

        personRepositoy.save(person);
        return;
    }

    @GetMapping("/remove")
    private String remove(@RequestParam(value = "id",defaultValue = "1",required = false) Integer id){
        personRepositoy.remove(id);
        return "true";
    }

    @GetMapping("/modify")
    private Person modify(@RequestParam(value = "id",defaultValue = "1",required = false) Integer id){
        return personRepositoy.modify(id);
    }

}
