package homework.thon.thatthon;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class ThatThonApplication {

    public static void main(String[] args) {
        SpringApplication.run(ThatThonApplication.class, args);
    }
}
